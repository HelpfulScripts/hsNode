hsNode 
========
[![npm version](https://badge.fury.io/js/hsnode.svg)](https://badge.fury.io/js/hsnode) 
[![Build status](https://ci.appveyor.com/api/projects/status/b6j3c52mckq2lpj8?svg=true)](https://ci.appveyor.com/project/HelpfulScripts/hsnode)
[![Built with Grunt](https://cdn.gruntjs.com/builtwith.svg)](https://gruntjs.com/) 
[![NPM License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://www.npmjs.com/package/hsnode)

Helpful Scripts functions that wrap some nodejs calls in Promise wrappers.

## Installation
Install `hsNode` from `npm` the standard way:
> `npm i hsnode`

See [docs](https://helpfulscripts.github.io/hsNode/indexGH.html#!/api/hsNode/0) for details