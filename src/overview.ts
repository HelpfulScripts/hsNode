/**
# hsNode

Helpful Scripts functions that wrap some nodejs calls in Promise wrappers.
[Github page](https://github.com/HelpfulScripts/hsNode)

 * ## Index of nodejs wrappers
 * - &nbsp; {@link cpUtil child_processs} child process execution
 * - &nbsp; {@link fsUtil file system} functions
 * - &nbsp; {@link httpUtil http} web server functions

 * ## Index of other node support functions
 * - &nbsp; {@link log log} logging support with per-module prefixes


*/

/** */